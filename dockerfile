FROM python:3.6.5

ADD . /schedule
WORKDIR /schedule

ENV TZ=Europe/Moscow
ENV IN_DOCKER=1
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN pip install -r requirements.txt
CMD ["python", "schedule.py"]
