import os

IN_DOCKER = bool(os.environ.get('IN_DOCKER', False))

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
LOG_PATH = os.path.join(ROOT_DIR, 'logs', 'log.txt')
DB_PATH = os.path.join(ROOT_DIR, 'db', 'db.sqlite3' if IN_DOCKER else 'db_host.sqlite3')
