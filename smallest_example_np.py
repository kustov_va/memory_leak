from time import sleep

import numpy as np


def do_the_job():
    arr = np.random.randint(0, 100, size=(100_000_000, 4))
    print(arr[0][1])
    del arr


def main():
    for _ in range(3):
        do_the_job()
        sleep(3)


if __name__ == '__main__':
    main()
