from time import sleep
import gc

import numpy as np
import pandas as pd


def do_the_job():
    df = pd.DataFrame(np.random.randint(0, 100, size=(100_000_000, 4)), columns=list('ABCD'), dtype='int64')
    print(df.loc[0]['A'])
    del df
    gc.collect()


def main():
    for _ in range(3):
        do_the_job()
        sleep(3)


if __name__ == '__main__':
    main()
