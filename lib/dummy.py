import sqlite3

import gc
import numpy as np
import pandas as pd

from lib.helpers import get_logger
from definitions import DB_PATH


class Foo:
    def __init__(self):
        self.logger = get_logger('foo')
        self.db_conn = sqlite3.connect(DB_PATH)
        self.df = pd.DataFrame(np.random.randint(0, 100, size=(100_000_000, 4)), columns=list('ABCD'), dtype='int64')

    def bar(self):
        self.logger.debug(f'Memory used by the df: {self.df.memory_usage(deep=True).sum()//1024**2} Mb')
        self.df.loc[0].to_sql('temp', self.db_conn, if_exists='append')
        print(self.df.loc[0])
        # del self.df
        # self.db_conn.close()


def do_the_job():
    foo = Foo()
    foo.bar()
    del foo
    gc.collect()


if __name__ == '__main__':
    do_the_job()
