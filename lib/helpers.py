import logging

from definitions import LOG_PATH


def get_logger(name: str, logging_level=logging.DEBUG) -> logging.Logger:
    # logger
    logger = logging.getLogger(name)
    logger.setLevel(logging_level)

    # if logger is just created it has no handlers yet
    if not len(logger.handlers):
        # filehandler
        fh = logging.FileHandler(filename=LOG_PATH)
        fh.setLevel(logging.DEBUG)

        # formatter
        file_formatter = logging.Formatter(style='%', fmt='[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')

        # attach everything
        fh.setFormatter(file_formatter)
        logger.addHandler(fh)

    return logger
