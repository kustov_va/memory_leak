# from time import sleep

from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.cron import CronTrigger

from lib.dummy import do_the_job


def main():
    scheduler = BlockingScheduler()
    scheduler.add_job(
        func=do_the_job,
        trigger=CronTrigger(second='*/15'),
        id='job',
        name='job',
        replace_existing=True)
    scheduler.start()
    # while True:
    #     do_the_job()
    #     sleep(3)


if __name__ == '__main__':
    main()
